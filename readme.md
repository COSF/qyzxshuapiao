# 轻院之星刷票刷热度

## 要求 与 安装

* IPProxyPool
    
    IPProxyPool是一个开源的 ip 代理池，具体点这里[IPProxyPool](https://github.com/qiyeboy/IPProxyPool)
    1. 安装 sqlite 数据库(一般系统内置): `apt-get install sqlite3` 
    2. 安装 requests,chardet,web.py,gevent psutil: `pip3 install requests chardet web.py sqlalchemy gevent psutil` 
    3. 安装lxml: `apt-get install python-lxml `

        注意：
        * python2 下的是 pip
        * 有时候使用的gevent版本过低会出现自动退出情况，请使用 pip install gevent --upgrade 更新)
        * 在 python3 中安装 web.py ，不能使用 pip ，直接下载 py3 版本的源码进行安装
* 本项目
    * python3
    * `pip3 install -r requirements.txt`

## 使用

* 按照 IPProxyPool 的文档，将它启动
* 在项目跟目录执行 `python3 main.py`

## 配置

要改变被刷票的人，修改 main.py 的:

```python
vote_url # 投票 url
view_url # 热度 url
```

