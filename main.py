import requests
import re
import yaml
import random
import linecache
import time
import os


PROXY_POOL_URL = 'http://0.0.0.0:8000/?types=0&country=%E5%9B%BD%E5%86%85'

def get_proxy():
    try:
        response = requests.get(PROXY_POOL_URL)
        if response.status_code == 200:
            reg = re.compile("\[\'(?P<ip>[^ ]*)\', (?P<port>[^ ]*), ")
            regMatch = reg.match(response.text)
            linebits = regMatch.groupdict()
            proxies = {
                'http':"",
                'https':"",
            }
            for k, v in linebits.items() :
                if k == "ip":
                    proxies["http"] += v + ":"
                    proxies["https"] += v + ":"
                else:
                    proxies["http"] += v
                    proxies["https"] += v
            return proxies
    except ConnectionError:
        return None

def del_ip(proxies):
    try:
        ip = proxies["http"]
        reg = re.compile("(?P<ip>[^ ]*):")
        regMatch = reg.match(ip)
        linebits = regMatch.groupdict()
        url = "http://0.0.0.0:8000/delete?ip="+linebits["ip"]
        r = requests.get(url)
        return r.text
    except ConnectionError:
            return None

def get_header():
    def get_randua():
        try:
            f = open("ua.dat")     
            i = random.randint(1,len(f.readlines()))
            f.close()
            randua = linecache.getline("ua.dat",i).strip('\n')
            return randua
        except ConnectionError:
            return None
    def set_randua():
        f = open("header.yml")
        header = yaml.load(f)
        header["user-agent"] = get_randua()
        return header
    return set_randua()

vote_url = "http://mr44pk.maowechat.cn/app/index.php?i=2&c=entry&rid=40&id=2473&do=vote&m=tyzm_diamondvote"
view_url = "http://mr44pk.maowechat.cn/app/index.php?i=2&c=entry&ty=pv&rid=40&id=2473&do=Count&m=tyzm_diamondvote"

i = 1
while i:
    # time.sleep(1)
    # os.system('clear')
    numFlag = False
    hotFlag = False
    headers = get_header()
    proxies = get_proxy()
    # proxies = {}
    print("已投",i-1,"票!\n正在使用的代理:",proxies)
    try:
        print("投票中...")
        r = requests.post(vote_url, headers = headers,proxies=proxies,timeout=3)
        print(r.text)
        if (r.status_code == 200):
            numFlag = True
            print("Num Good!")
            i = i+1
        else:
            print("Num Bad!")
            continue
        r = requests.get(view_url, headers = headers,proxies=proxies,timeout=3)
        if (r.status_code == 200):
            hotFlag = True
            print("Hot Good!")
        else:
            print("Hot Bad!")
    except requests.exceptions.ConnectTimeout:
        print("连接超时!")
        out = True
        continue
    except requests.exceptions.Timeout:
        print("超时!")
        out = True
        continue
    except requests.exceptions.ProxyError:
        del_ip(proxies)
        print("代理错误!")
        out = True
        continue 
    except requests.exceptions.ChunkedEncodingError:
        print("编码错误!")
        out = True
        continue 
